module.exports = {
    pgUser: process.env.PGUSER,
    pgHost: process.env.PGHOST,
    pgPort: process.env.PGPORT,
    pgDatabase: process.env.PGDATABASE,
    pgPassword: process.env.PGPASSWORD
};