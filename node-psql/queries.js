import { Client } from "pg";

const pgClient = new Client({
    password: "postgres",
    user: "postgres",
    host: "postgres",
  });
  

pgClient.on('error', () => {
    console.log("Lost pg connection");
});

const createUserTableIfNotExist = async (request, response) => {
    pgClient.query('CREATE TABLE IF NOT EXISTS users(ID  SERIAL PRIMARY KEY, NAME TEXT NOT NULL,EMAIL TEXT NOT NULL)', (error, result) => {
        if (error) {
            throw error;
        }
        console.log("created the users table");
    });
}

const getUsers = async (request, response) => {
    pgClient.query('SELECT * from  users order by id ASC', (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).json(results.rows);
    });
}

const getUserById = async (request, response) => {
    const id = parseInt(request.params.id)
    console.log("id " + id);
    pgClient.query('SELECT * from users where id = $1', [id], (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).json(results.row);
    });
}

const createUser = (request, response) => {
    const { name, email } = request.body;
    console.log(request.body);
    console.log("name " + name + " and email " + email);
    pgClient.query("INSERT INTO users (name, email) VALUES ('" + name + "', '" + email + "')",
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(201).send(`User added with ID: ${results.insertId}`)
        })
}

const updateUser = (request, response) => {
    const id = parseInt(request.params.id);
    const { name, email } = request.body;
    pgClient.query('UPDATE users SET name=$1, email=$2 WHERE id=$3', [name, email, id], (error, results) => {
        if (error) {
            throw error;
        }
        response.status(200).send('User modified from the id :' + id);
    });
}

const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)

    pgClient.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).send(`User deleted with ID: ${id}`)
    })
}


module.exports = { createUser, createUserTableIfNotExist, getUsers, deleteUser, getUserById, updateUser };