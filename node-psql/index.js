const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const db = require('./queries');
const keys = require('./keys');
const port = 3000;
console.log(keys);
const app = express();
app.use(cors());
app.use(bodyParser.json());

db.createUserTableIfNotExist();

app.get("/", async (request, response) => {
    response.json({ info: 'Node.js, Express info, and Postgres API' })
});

app.get('/users', db.getUsers);
app.get('/users/:id', db.getUserById);
app.post('/users', db.createUser);
app.put('/users/:id', db.updateUser);   
app.delete('/users/:id', db.deleteUser);



app.listen(port, () => {
    console.log(`App is running on port ${port}`);
})